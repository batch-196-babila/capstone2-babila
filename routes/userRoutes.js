// const router = require("../../booking-system-api/routes/courseRoutes");

const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// register user
router.post('/', userControllers.registerUser);

// login
router.post('/login', userControllers.loginUser);

// Retrieve user details
router.get('/getUserDetails', verify, userControllers.getUserDetails);

// Update user role to admin
router.put('/updateAdmin/:userId', verify, verifyAdmin, userControllers.updateAdmin);

// Create one order for logged in users
router.post('/checkoutSingleOrder', verify, userControllers.createSingleOrder);

// Create one order for logged in users
router.post('/checkoutMultipleOrders', verify, userControllers.createMultipleOrders);

// Get authenticated user's order
router.get('/viewOrders', verify, userControllers.getUserOrders);

// Get all orders from all users (admin only)
router.get('/viewAllOrders', verify, verifyAdmin, userControllers.getAllOrders);

// Retrieve user details
// router.get('/getProductPerOrder', verify, userControllers.getProductPerOrder);

module.exports = router;
