const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Create product (admin only)
router.post('/', verify, verifyAdmin, productControllers.createProduct);

// Retrieve active products
router.get('/active', productControllers.getActiveProducts);

// Retrieve single product
router.get('/getSingleProduct/:productId', productControllers.getSingleProduct);

// Update product (admin only)
router.put('/update/:productId', verify, verifyAdmin, productControllers.updateProduct);

// Update product (admin only)
router.delete('/archive/:productId', verify, verifyAdmin, productControllers.archiveProduct);

/* Stretch goal */

// Activate product (admin only)
router.put('/activate/:productId', verify, verifyAdmin, productControllers.activateProduct);

module.exports = router;