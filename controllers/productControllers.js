const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");
const { response } = require("express");

// Create product (admin only)
module.exports.createProduct = (request, response) => {
    let createProduct = new Product({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    });
    
    createProduct.save()
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// Retrieve All Active Products
module.exports.getActiveProducts = (request, response) => {
    Product.find({isActive: true})
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// Retrieve Single Product
module.exports.getSingleProduct = (request, response) => {
    Product.findById(request.params.productId)
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// Update Product (admin only)
module.exports.updateProduct = (request, response) => {
    // console.log(request.params.productId);

    let update = {
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    }

    Product.findByIdAndUpdate(request.params.productId, update, {new: true})
    .then(updateResult => response.send(updateResult))
    .catch(error => res.send(error))
}

// Archive Product (admin only)
module.exports.archiveProduct = (request, response) => {
    // console.log(request.params.productId);

    let archive = {
        isActive: false
    }

    Product.findByIdAndUpdate(request.params.productId, archive, {new: true})
    .then(archiveResult => response.send(archiveResult))
    .catch(error => res.send(error))
}

/* Strect goal */

// Activate Product (admin only)
module.exports.activateProduct = (request, response) => {
    // console.log(request.params.productId);

    let archive = {
        isActive: true
    }

    Product.findByIdAndUpdate(request.params.productId, archive, {new: true})
    .then(activateResult => response.send(activateResult))
    .catch(error => res.send(error))
}