const express = require("express");
const mongoose = require("mongoose");
const app = express();
app.use(express.json());
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://admin:admin123@cluster0.5eaawok.mongodb.net/ecommerceAPI?retryWrites=true&w=majority", {
    useNewURLParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "Database connection error, please contact support."));

db.once('open', () => console.log("Connected to database."));

const userRoutes = require("./routes/userRoutes");
app.use('/users', userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use('/products', productRoutes);

app.listen(port, () => console.log(`E-commerce API running at port 4000.`));